package me.pushy.player;

import lombok.Getter;
import lombok.Setter;
import me.pushy.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marc / iPushy_
 */
public class AccountPlayer  {

    @Getter @Setter private int userID, totalCash;
    @Getter @Setter private Player player;

    private PlayerHandler playerHandler;
    private Main main;
    @Getter public List<Integer> availableAccounts;

    public AccountPlayer(Main main, PlayerHandler playerHandler, Player player, int userID, int totalCash, List<Integer> availableAccounts) {
        this.player = player;
        this.userID = userID;
        this.totalCash = totalCash;
        this.main = main;
        this.playerHandler = playerHandler;
        this.availableAccounts = availableAccounts;
    }
    public boolean canMakePayment(int amountToPay){
        if((getTotalCash() - amountToPay) < 0) return false;
        return true;
    }
    public void makePlayerPayment(Player target, int amountToPay){
        if(!canMakePayment(amountToPay)){
            this.player.sendMessage(this.main.getPrefix() + "Du hast dafür leider zu wenig Geld dabei!");
            return;
        }
        if(this.playerHandler.getUser(target) == null){
            this.player.sendMessage(this.main.getPrefix() + "Dieser Spieler ist leider gerade nicht online!");
            return;
        }
        if(target.equals(getPlayer())){
            this.player.sendMessage(this.main.getPrefix() + "Du kannst dir selber kein Geld überweisen!");
            return;
        }
        AccountPlayer accountPlayer = this.playerHandler.getUser(target);
        accountPlayer.setTotalCash(accountPlayer.getTotalCash() + amountToPay);
        setTotalCash(getTotalCash() - amountToPay);
        sendSucessMessage(getPlayer(), target, amountToPay);
    }
    public void makePayment(int amountToPay){
        setTotalCash(getTotalCash() - amountToPay);
    }
    public int getAvailableAccountsCount(){
        return availableAccounts.size();
    }
    private void sendSucessMessage(Player from, Player to, int amountToPay){
        from.sendMessage(this.main.getPrefix() + "Du hast dem Spieler §e" + to.getName() + " §7" + amountToPay + " §7überwiesen!");
        to.sendMessage(this.main.getPrefix() + "§7Dir wurde §7" + amountToPay + " §7von §e" + from.getName() + " §7überwiesen!");
    }
}