package me.pushy.player;

import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.main.Main;
import me.pushy.mysql.AsyncMySQL;
import me.pushy.util.Callback;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Marc / iPushy_
 */

public class PlayerHandler implements Listener {

    private static Set<AccountPlayer> users;
    private static PreparedStatement insertUser;

    private Main main;
    private BankHandler bankHandler;

    public PlayerHandler(Main main) {
        this.main = main;
        this.bankHandler = new BankHandler(main, this);
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent e) {
        initUser(e.getPlayer());
        initPlayerAccounts(getUser(e.getPlayer()));
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        saveData(p);
        unregisterUser(p);
    }
    public void initPlayerAccounts(AccountPlayer p) {
        p.getAvailableAccounts().forEach(i -> {
            this.bankHandler.loadAccountByID(i);
        });
    }

    private void unloadPlayerAccounts(AccountPlayer p) {
        List<Account> toUnload = new ArrayList<>();
        p.getAvailableAccounts().forEach(i -> {
            Account account = this.bankHandler.getAccountByID(Integer.valueOf(i));
            if (account != null) {
                this.bankHandler.saveCredit(account.getAccountID(), account.getCredit(), cash -> {});
                toUnload.add(account);
            }
        });
        toUnload.forEach(account -> {
            this.bankHandler.unloadAccount(account.getAccountID());
        });
    }

    public void onDisable() {
        Bukkit.getOnlinePlayers().forEach(all -> {
            saveData(all);
            unregisterUser(all);
        });
    }

    public void initUsers() {
        users = new HashSet<>();
        Bukkit.getOnlinePlayers().forEach(all -> {
            initUser(all);
        });
    }

    public void initSQL() {
        AsyncMySQL.MySQL syncSQL = this.main.getSQL().getMySQL();
        syncSQL.queryUpdate("create table if not exists users("
                + "userID int(6) primary key auto_increment not null, "
                + "userUUID varchar(36) not null, "
                + "availableAccounts varchar(80) not null, "
                + "cash int(10) not null)");
        Connection con = syncSQL.getConnection();
        try {
            insertUser = con.prepareStatement("insert into users(userUUID, cash, availableAccounts) values(?, ?, ?)", java.sql.Statement.RETURN_GENERATED_KEYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveData(Player p) {
        saveCash(p, getUser(p).getTotalCash(), cash -> {});
        saveAvailableAccounts(p, getUser(p).getAvailableAccounts(), cash -> {});
    }

    public void initUser(Player p) {
        ResultSet res = this.main.getSQL().getMySQL().query("select * from users where userUUID='" + p.getUniqueId().toString() + "'");
        try {
            if (res.next()) {
                int userID = res.getInt("userID");
                int cash = res.getInt("cash");

                String availableAccounts = res.getString("availableAccounts").replace(" ", "").replace("  ", "");
                List<String> accounts = new ArrayList<>(Arrays.asList(availableAccounts.split(",")));
                List<Integer> accountPlayerAccounts = new ArrayList<>();
                for (String account : accounts) {
                    if (!account.equals("")) {
                        accountPlayerAccounts.add(Integer.valueOf(account));
                    }
                }
                AccountPlayer accountPlayer = new AccountPlayer(this.main, this, p, userID, cash, accountPlayerAccounts);
                registerUser(accountPlayer);
                res.close();
            } else {
                insertUser.setString(3, "");
                insertUser.setInt(2, 50);
                insertUser.setString(1, p.getUniqueId().toString());
                res.close();

                ResultSet resultSet = this.main.getSQL().getMySQL().queryKey(insertUser);
                resultSet.next();

                int userID = resultSet.getInt(1);

                List<Integer> accounts = new ArrayList<>();
                AccountPlayer accountPlayer = new AccountPlayer(this.main, this, p, userID, 50, accounts);
                registerUser(accountPlayer);
                resultSet.close();
                initUser(p);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getTotalPlayerMoney(Player p) {
        AccountPlayer accountPlayer = getUser(p);
        int totalMoney = 0;
        for (Account account : this.main.availableAccounts) {
            totalMoney = totalMoney + account.getCredit();
        }
        totalMoney = totalMoney + accountPlayer.getTotalCash();
        return totalMoney;
    }

    public AccountPlayer getUser(Player p) {
        Set<AccountPlayer> sync = Collections.synchronizedSet(users);
        synchronized (sync) {
            for (AccountPlayer accountPlayer : sync) {
                if (accountPlayer.getPlayer().equals(p))
                    return accountPlayer;
            }
        }
        return null;
    }

    private void registerUser(AccountPlayer accountPlayer) {
        Set<AccountPlayer> synch = Collections.synchronizedSet(users);
        synchronized (synch) {
            synch.add(accountPlayer);
        }
    }

    private void unregisterUser(Player p) {
        AccountPlayer accountPlayer = getUser(p);
        unloadPlayerAccounts(accountPlayer);
        Set<AccountPlayer> synch = Collections.synchronizedSet(users);
        synchronized (synch) {
            synch.remove(accountPlayer);
        }
    }

    public void saveCash(Player p, int cashToSave, Callback<ResultSet> id) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement updateStatement = con.prepareStatement("UPDATE users SET cash= '" + cashToSave + "' WHERE userUUID= '" + p.getUniqueId().toString() + "';", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().queryKey(updateStatement, res -> {
                id.onReceive(res);
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveAvailableAccounts(Player p, List<Integer> listToSave, Callback<ResultSet> id) {
        try {
            String s = String.valueOf(listToSave).replace("[", "").replace("]", "");
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement updateStatement = con.prepareStatement("UPDATE users SET availableAccounts= '" + s + "' WHERE userUUID= '" + p.getUniqueId().toString() + "';", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().queryKey(updateStatement, res -> {
                id.onReceive(res);
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
