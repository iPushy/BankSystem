package me.pushy.main;

import lombok.Getter;
import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.commands.account.AccountCommands;
import me.pushy.commands.account.subcommands.AccountInformationCommand;
import me.pushy.commands.account.subcommands.ListAccountsCommand;
import me.pushy.commands.account.subcommands.SaveDataCommand;
import me.pushy.commands.bank.BankCommands;
import me.pushy.commands.bank.subcommands.BankInformationCommand;
import me.pushy.commands.bank.subcommands.BankStatementCommand;
import me.pushy.commands.bank.subcommands.CreateBankCommand;
import me.pushy.commands.bank.subcommands.DeleteBankComand;
import me.pushy.commands.transaction.TransactionCommands;
import me.pushy.commands.transaction.subcommands.PayMoneyFromBankToUser;
import me.pushy.commands.transaction.subcommands.PayMoneyToBankCommand;
import me.pushy.commands.transaction.subcommands.PayMoneyToUserUserCommand;
import me.pushy.commands.transaction.subcommands.TransferCommand;
import me.pushy.config.ConfigManager;
import me.pushy.inventory.AccountInventoryInteract;
import me.pushy.misc.AutoSave;
import me.pushy.mysql.AsyncMySQL;
import me.pushy.player.PlayerHandler;
import me.pushy.scoreboard.handler.MyScoreboardManager;
import me.pushy.transactions.TransactionHandler;
import me.pushy.util.Constants;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Marc / iPushy_
 */
public class Main extends JavaPlugin {

    private AsyncMySQL sql;
    private ConfigManager configManager;
    private PlayerHandler playerHandler;
    private BankHandler bankHandler;
    private MyScoreboardManager myScoreboardManager;
    private AutoSave autoSave;
    private TransactionHandler transactionHandler;

    @Getter
    private String prefix;

    public List<Account> availableAccounts;

    @Override
    public void onEnable() {
        this.availableAccounts = new ArrayList<>();
        this.configManager = new ConfigManager(this);
        this.playerHandler = new PlayerHandler(this);
        this.bankHandler = new BankHandler(this, playerHandler);
        this.myScoreboardManager = new MyScoreboardManager(this);
        this.autoSave = new AutoSave(this);
        this.transactionHandler = new TransactionHandler(this);

        initMySQLConfig();
        registerEvents();
        registerCommands();

        try {
            initSQL();
            this.playerHandler.initSQL();
            this.bankHandler.initSQL();
            this.transactionHandler.initSQL();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.prefix = "§eBank §8| §7";

        this.playerHandler.initUsers();
        for (Player p : Bukkit.getOnlinePlayers()) {
            this.playerHandler.initPlayerAccounts(this.playerHandler.getUser(p));
        }
        this.myScoreboardManager.initScoreboard();
        this.autoSave.startAutoSave();
        this.myScoreboardManager.update();
    }

    @Override
    public void onDisable() {
        this.sql.getMySQL().closeConnection();
    }

    public boolean registerCommand(Command command) {
        if (command == null)
            return false;
        String commandName = command.getCommandName();
        String[] commandAlias = command.getAlias();
        getCommand(commandName).setExecutor(command);
        getCommand(commandName).setAliases(Arrays.asList(commandAlias));
        return true;
    }

    private void registerEvents() {
        Bukkit.getPluginManager().registerEvents(new PlayerHandler(this), this);
        Bukkit.getPluginManager().registerEvents(new AccountInventoryInteract(), this);
        Bukkit.getPluginManager().registerEvents(new MyScoreboardManager(this), this);
    }

    private void registerCommands() {
        registerCommand(new BankCommands(this));
        registerCommand(new AccountCommands(this));
        registerCommand(new TransactionCommands(this));
    }

    private void initMySQLConfig() {
        FileConfiguration fileConfiguration = this.configManager.getConfig(Constants.MYSQL_CONFIG_NAME);
        fileConfiguration.addDefault("mysql.host", "localhost");
        fileConfiguration.addDefault("mysql.user", "root");
        fileConfiguration.addDefault("mysql.database", "database");
        fileConfiguration.addDefault("mysql.password", "password");
        fileConfiguration.addDefault("mysql.port", 3306);
        fileConfiguration.options().copyDefaults(true);
        this.configManager.saveConfig(Constants.MYSQL_CONFIG_NAME);
    }

    private void initSQL() throws Exception {
        String host = this.configManager.getConfig(Constants.MYSQL_CONFIG_NAME).getString("mysql.host");
        String user = this.configManager.getConfig(Constants.MYSQL_CONFIG_NAME).getString("mysql.user");
        String pw = this.configManager.getConfig(Constants.MYSQL_CONFIG_NAME).getString("mysql.password");
        String db = this.configManager.getConfig(Constants.MYSQL_CONFIG_NAME).getString("mysql.database");
        int port = this.configManager.getConfig(Constants.MYSQL_CONFIG_NAME).getInt("mysql.port");
        this.sql = new AsyncMySQL(this, host, port, user, pw, db);
        this.sql.getMySQL().openConnection();
    }

    public AsyncMySQL getSQL() {
        return sql;
    }
}
