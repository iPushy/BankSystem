package me.pushy.bank;

import me.pushy.account.Account;
import me.pushy.main.Main;
import me.pushy.mysql.AsyncMySQL;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import me.pushy.util.Callback;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marc / iPushy_
 */
public class BankHandler {

    private Main main;

    private static PreparedStatement insertBank;
    private PlayerHandler playerHandler;

    public BankHandler(Main main, PlayerHandler playerHandler) {
        this.main = main;
        this.playerHandler = playerHandler;
    }

    public void initSQL() {
        AsyncMySQL.MySQL syncSQL = this.main.getSQL().getMySQL();
        syncSQL.queryUpdate(
                "create table if not exists bank("
                        + "accountID int(10) primary key auto_increment not null, "
                        + "ownerUUID varchar(36) not null, "
                        + "accountNumber int(10) not null, "
                        + "credit int(10) not null)");
        Connection syncSQLConnection = syncSQL.getConnection();
        try {
            insertBank = syncSQLConnection.prepareStatement("insert into bank(ownerUUID, accountNumber, credit) values(?, ?, ?)", java.sql.Statement.RETURN_GENERATED_KEYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new account number
     *
     * @param length the length of the account number
     * @return the generated account number
     */
    public String createNewAccountNumber(int length) {
        int i = 1;
        char[] chars = "0123456789".toCharArray();
        StringBuilder stringbuilder = new StringBuilder();
        SecureRandom secureRandom = new SecureRandom();
        while (i <= length) {
            char aChar = chars[secureRandom.nextInt(chars.length)];
            stringbuilder.append(aChar);
            i++;
        }
        return stringbuilder.toString();
    }

    /**
     * Checks if the given account is loaded on the server
     *
     * @param accountID the account id
     * @return true if it´s loaded on the server
     */
    public boolean accountIsLoaded(int accountID) {
        for (Account account : this.main.availableAccounts) {
            if (account.getAccountID() == accountID)
                return true;
        }
        return false;
    }

    /**
     * Loads an account
     *
     * @param accountNumber the account number to load
     */
    public void loadAccount(int accountNumber) {
        bankExists(accountNumber, exists -> {
            if (exists) {
                getOwnerUUID(accountNumber, ownerUUID -> {
                    getCredit(accountNumber, credit -> {
                        getAccountByAccountNumber(accountNumber, accountID -> {
                            Account account = new Account(this.main, java.util.UUID.fromString(ownerUUID), credit, accountID, accountNumber);
                            this.main.availableAccounts.add(account);
                        });
                    });
                });
            }
        });
    }

    /**
     * Loads an account with the given id
     *
     * @param accountID the account id
     */
    public void loadAccountByID(int accountID) {
        bankExistsByID(accountID, exists -> {
            if (exists) {
                getOwnerUUIDByID(accountID, ownerUUID -> {
                    getCreditByID(accountID, credit -> {
                        getAccountNumberByID(accountID, accountNumber -> {
                            Account account = new Account(this.main, java.util.UUID.fromString(ownerUUID), credit, accountID, accountNumber);
                            this.main.availableAccounts.add(account);
                        });
                    });
                });
            }
        });
    }

    /**
     * Unloads a certain account
     *
     * @param accountID the account id
     */
    public void unloadAccount(int accountID) {
        List<Account> removeAccounts = new ArrayList<>();
        this.main.availableAccounts.stream().filter(account -> account.getAccountID() == accountID).forEach(account -> removeAccounts.add(account));
        removeAccounts.forEach(account -> this.main.availableAccounts.remove(account));
    }

    /**
     * Returns an account with the given id
     *
     * @param accountID the id
     * @return the account
     */
    public Account getAccountByID(int accountID) {
        return this.main.availableAccounts.stream().filter(acc -> acc.getAccountID() == accountID).findFirst().orElse(null);
    }

    /**
     * Returns an account with the given account number
     *
     * @param accountNumber the account number
     * @return the account
     */
    public Account getAccountByAccountNumber(int accountNumber) {
        return this.main.availableAccounts.stream().filter(acc -> acc.getAccountNumber() == accountNumber).findFirst().orElse(null);
    }

    /**
     * Creates a new bank
     *
     * @param p the player
     */
    public void createBank(Player p) {
        String accountNumber = createNewAccountNumber(6).toString();
        p.sendMessage(this.main.getPrefix() + "Bank wird geladen...");
        bankExists(Integer.valueOf(accountNumber), exists -> {
            if (!exists) {
                try {
                    insertBank.setString(1, p.getUniqueId().toString());
                    insertBank.setString(2, accountNumber);
                    insertBank.setInt(3, 0);

                    ResultSet resultSet = this.main.getSQL().getMySQL().queryKey(insertBank);
                    resultSet.next();
                    int accountID = resultSet.getInt(1);
                    resultSet.close();

                    Account bankAccount = new Account(main, p.getUniqueId(), 0, accountID, Integer.valueOf(accountNumber));
                    this.main.availableAccounts.add(bankAccount);

                    AccountPlayer accountPlayer = this.playerHandler.getUser(p);
                    List<Integer> accounts = accountPlayer.getAvailableAccounts();
                    accounts.add(accountID);
                    sendSuccess(p, accountID, accountNumber, 0);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                p.sendMessage(this.main.getPrefix() + "Ein Fehler ist aufgetreten! Versuche es erneut!");
            }
        });
    }

    /**
     * Deletes a bank with a certain accountID
     *
     * @param accountID the account id
     * @param id        the id
     */
    public void deleteBank(int accountID, Callback<ResultSet> id) {
        bankExistsByID(accountID, exists -> {
            if (exists) {
                try {
                    Connection con = this.main.getSQL().getMySQL().getConnection();
                    PreparedStatement updateStatement = con.prepareStatement("delete from bank where accountID='" + accountID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
                    main.getSQL().queryKey(updateStatement, res -> {
                        id.onReceive(res);
                    });
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Checks if the bank exists
     *
     * @param accountNumber the account number
     * @param callback      true if the bank exists
     */
    public void bankExists(int accountNumber, Callback<Boolean> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select * from bank where accountNumber='" + accountNumber + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                try {
                    if (res.next()) {
                        callback.onReceive(true);
                    } else {
                        callback.onReceive(false);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Checks if the bank exists
     *
     * @param accountID the account id
     * @param callback  true if the bank exists
     */
    public void bankExistsByID(int accountID, Callback<Boolean> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select * from bank where accountID='" + accountID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                try {
                    if (res.next()) {
                        callback.onReceive(true);
                    } else {
                        callback.onReceive(false);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Saves the credit of
     *
     * @param bankID       the bank id
     * @param creditToSave the credit to save
     * @param id           the id
     */
    public void saveCredit(int bankID, int creditToSave, Callback<ResultSet> id) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement updateStatement = con.prepareStatement("update bank set credit= '" + creditToSave + "' WHERE accountID= '" + bankID + "';", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().queryKey(updateStatement, res -> {
                id.onReceive(res);
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Returns the owner uuid
     *
     * @param accountNumber the account number
     * @param callback      the uuid
     */
    public void getOwnerUUID(int accountNumber, Callback<String> callback) { //ID = AccountNumber
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select ownerUUID from bank where accountNumber='" + accountNumber + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                String accountNumberInt = "";
                try {
                    if (res.next()) {
                        accountNumberInt = res.getString("ownerUUID");
                    }
                    callback.onReceive(accountNumberInt);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Returns the credit from an account
     *
     * @param accountNumber the account number
     * @param callback      the amount
     */
    public void getCredit(int accountNumber, Callback<Integer> callback) { //ID = AccountNumber
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select credit from bank where accountNumber='" + accountNumber + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                int accountNumberInt = 0;
                try {
                    if (res.next()) {
                        accountNumberInt = res.getInt("accountNumber");
                    }
                    callback.onReceive(accountNumberInt);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Returns the account number
     *
     * @param bankID   the bank id
     * @param callback the number
     */
    public void getAccountNumberByID(int bankID, Callback<Integer> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select accountNumber from bank where accountID='" + bankID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                int accountNumber = 0;
                try {
                    if (res.next()) {
                        accountNumber = res.getInt("accountNumber");
                    }
                    callback.onReceive(accountNumber);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Returns the owner by an id
     *
     * @param bankID   the id
     * @param callback the uuid
     */
    public void getOwnerUUIDByID(int bankID, Callback<String> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select ownerUUID from bank where accountID='" + bankID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                String uuid = "";
                try {
                    if (res.next()) {
                        uuid = res.getString("ownerUUID");
                    }
                    callback.onReceive(uuid);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Returns the credit by an id
     *
     * @param bankID   the bank id
     * @param callback the credit
     */
    public void getCreditByID(int bankID, Callback<Integer> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select credit from bank where accountID='" + bankID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                int credit = 0;
                try {
                    if (res.next()) {
                        credit = res.getInt("credit");
                    }
                    callback.onReceive(credit);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Returns an account by an account number
     *
     * @param accountNumber the account number
     * @param callback      the accountid
     */
    public void getAccountByAccountNumber(int accountNumber, Callback<Integer> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select accountID from bank where accountNumber='" + accountNumber + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                int accountID = 0;
                try {
                    if (res.next()) {
                        accountID = res.getInt("accountID");
                    }
                    callback.onReceive(accountID);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Checks if an account is loaded
     *
     * @param accountNumber the account number
     * @return true if loaded
     */
    public boolean accountIsLoadedByAccountNumber(int accountNumber) {
        for (Account account : this.main.availableAccounts) {
            if (account.getAccountNumber() == accountNumber)
                return true;
        }
        return false;
    }

    /**
     * Sends the default success message
     *
     * @param p         the player
     * @param accountID the account id
     * @param pin       the generated pin
     * @param credit    the credit
     */
    private void sendSuccess(Player p, int accountID, String pin, int credit) {
        p.sendMessage(this.main.getPrefix() + "Dein neues Konto:");
        p.sendMessage(this.main.getPrefix() + "KontoID: §e" + accountID);
        p.sendMessage(this.main.getPrefix() + "Kontonummer: §e" + pin);
        p.sendMessage(this.main.getPrefix() + "Guthaben: §e" + credit);
    }
}
