package me.pushy.scoreboard.handler;

import me.pushy.main.Main;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import me.pushy.scoreboard.rows.Row;
import me.pushy.util.Triplet;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Marc / iPushy_
 */

public class MyScoreboardManager implements Listener {

    private Main main;
    private PlayerHandler playerHandler;

    private static Map<Player, Triplet<Integer, Integer, Integer>> saves = new HashMap<>();

    public MyScoreboardManager(Main main) {
        this.main = main;
        this.playerHandler = new PlayerHandler(main);

    }

    public void initScoreboard() {
        Bukkit.getOnlinePlayers().forEach(all -> createAndApplyScoreboardFor(all));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(PlayerJoinEvent e) {
        createAndApplyScoreboardFor(e.getPlayer());
    }

    public void createAndApplyScoreboardFor(Player p) {
        AccountPlayer accountPlayer = this.playerHandler.getUser(p);

        String boardName = "§eBankSystem";
        String moneyLayout = "§7Bargeld: §e%CASH%";
        String accountsLayout = "§7Konten: §e%ACCOUNTS%";
        String totalMoneyLayout = "§7Geld: §e%MONEY%";

        MyScoreboard sb = new MyScoreboard(boardName);
        int accounts = accountPlayer.getAvailableAccountsCount();
        int cash = accountPlayer.getTotalCash();
        int totalMoney = this.playerHandler.getTotalPlayerMoney(p);

        Triplet<Integer, Integer, Integer> save = new Triplet<>(cash, accounts, totalMoney);
        saves.put(p, save);
        sb.addRow(1, new Row(sb, "§e"));
        Row moneyRow = new Row(sb, moneyLayout.replace("%CASH%", "" + cash));
        sb.addRow(2, moneyRow);
        Row accountsRow = new Row(sb, accountsLayout.replace("%ACCOUNTS%", "" + accounts));
        sb.addRow(3, accountsRow);
        sb.addRow(4, new Row(sb, " §e"));
        Row totalMoneyRow = new Row(sb, totalMoneyLayout.replace("%MONEY%", "" + totalMoney));
        sb.addRow(5, totalMoneyRow);

        sb.apply(p);
    }

    public void update() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(main, () -> {
            Bukkit.getOnlinePlayers().forEach(all -> {
                AccountPlayer accountPlayer = playerHandler.getUser(all);
                Triplet<Integer, Integer, Integer> outcome = saves.get(all);
                int cashOld = outcome.getFirst();
                int accountsOld = outcome.getSecond();
                int totalMoneyOld = outcome.getThird();
                int account = accountPlayer.getAvailableAccountsCount();
                int cash = accountPlayer.getTotalCash();
                int totalMoney = playerHandler.getTotalPlayerMoney(all);
                if (cashOld != cash || totalMoney != totalMoneyOld || accountsOld != account) {
                    createAndApplyScoreboardFor(all);
                }
            });
        }, 0L, 80L);
    }
}
