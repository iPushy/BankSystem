package me.pushy.scoreboard.rows;

import lombok.Getter;
import lombok.Setter;
import me.pushy.scoreboard.handler.MyScoreboard;
import org.bukkit.scoreboard.Score;

/**
 * Created by Marc / iPushy_
 */

public class Row implements Comparable<Row> {

	protected MyScoreboard scoreboard;
	protected Score score;
	@Getter private String entry;
	@Getter @Setter private int index;
	
	public Row(MyScoreboard scoreboard, String entry){
		this.scoreboard = scoreboard;
		score = this.scoreboard.getSidebar().getScore(entry);
		this.entry = entry;
	}
	public void setText(String con){
		scoreboard.getScoreboard().resetScores(entry);
		this.entry = con;
		score = this.scoreboard.getSidebar().getScore(entry);
		score = this.scoreboard.getSidebar().getScore(entry);
		scoreboard.update();
	}
	public void setScore(int scoreVal){
		score.setScore(scoreVal);
	}
	@Override
	public int compareTo(Row o) {
		return o.index - this.index;
	}
}
