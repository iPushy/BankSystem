package me.pushy.util;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Marc / iPushy_
 */

@Getter
@Setter
public class Tuple<T, E> {

    private T first;
    private E second;

    public Tuple(T first, E second) {
        this.first = first;
        this.second = second;
    }

}
