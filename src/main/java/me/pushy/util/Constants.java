package me.pushy.util;

/**
 * Created by Marc / iPushy_
 */
public class Constants {

    public static final String MYSQL_CONFIG_NAME = "MySQL";

    public static final int AUTO_SAVE_CONSTANT = 130;
    public static final int AUTO_SAVE_DELAY = 50;
}
