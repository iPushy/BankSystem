package me.pushy.util;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Marc / iPushy_
 */

@Getter
@Setter
public class Triplet<T, E, F> {

    private T first;
    private E second;
    private F third;

    public Triplet(T first, E second, F third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }
}
