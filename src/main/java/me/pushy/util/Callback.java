package me.pushy.util;

/**
 * Created by Marc / iPushy_
 */

public interface Callback<T> {

	void onReceive(T t);
	
}
