package me.pushy.config;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import me.pushy.main.Main;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Created by Marc / iPushy_
 */
public class ConfigManager {

    private HashMap<String, FileConfiguration> configurations = new HashMap<String, FileConfiguration>();

    private Main main;

    public ConfigManager(Main main) {
        this.main = main;
    }

    public boolean saveConfig(String name) {
        if (!configurations.containsKey(name)) {
            return false;
        }
        FileConfiguration conf = configurations.get(name);
        File cFile = new File(main.getDataFolder(), name + ".yml");
        try {
            cFile.createNewFile();
            conf.save(cFile);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public FileConfiguration getConfig(String name) {
        if (configurations.containsKey(name)) {
            return configurations.get(name);
        }
        main.getDataFolder().mkdirs();
        File cFile = new File(main.getDataFolder(), name + ".yml");
        try {
            cFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileConfiguration conf = YamlConfiguration.loadConfiguration(cFile);
        configurations.put(name, conf);
        return conf;
    }
}
