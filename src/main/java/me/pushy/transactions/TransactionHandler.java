package me.pushy.transactions;

import me.pushy.main.Main;
import me.pushy.mysql.AsyncMySQL;
import me.pushy.util.Callback;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marc Sogl / iPushy_
 */

public class TransactionHandler {

    private Main main;

    private static PreparedStatement insertTransaction;

    public TransactionHandler(Main main) {
        this.main = main;
    }
    public void initSQL() {
        AsyncMySQL.MySQL syncSQL = this.main.getSQL().getMySQL();
        syncSQL.queryUpdate("create table if not exists transaction("
                + "transactionsID int(10) primary key auto_increment not null, "
                + "accountNumber int(15) not null, "
                + "fromAccount int(15) not null, "
                + "toAccount int(15) not null)");
        Connection con = syncSQL.getConnection();
        try {
            insertTransaction = con.prepareStatement("insert into transaction(accountNumber, fromAccount, toAccount) values(?, ?, ?)", java.sql.Statement.RETURN_GENERATED_KEYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void onAddNewTransaction(int accountNumber, int from, int to){
        try {
            insertTransaction.setInt(1, accountNumber);
            insertTransaction.setInt(2, from);
            insertTransaction.setInt(3, to);
            this.main.getSQL().getMySQL().queryKey(insertTransaction);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void getAllTransactionsIDs(int accountNumber, Callback<List<Integer>> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select transactionsID from transaction where accountNumber='" + accountNumber + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                List<Integer> output = new ArrayList<Integer>();
                try {
                    while (res.next()) {
                        output.add(res.getInt("transactionsID"));
                    }
                    callback.onReceive(output);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
    public void getLimitTransactionsID(int accountNumber, Callback<List<Integer>> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select transactionsID from transaction where accountNumber='" + accountNumber + "' ORDER BY transactionsID DESC LIMIT 10", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                List<Integer> output = new ArrayList<Integer>();
                try {
                    while (res.next()) {
                        output.add(res.getInt("transactionsID"));
                    }
                    callback.onReceive(output);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
    public void getFromByTransactionsID(int transactionsID, Callback<Integer> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select fromAccount from transaction where transactionsID='" + transactionsID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                int from = 0;
                try {
                    if (res.next()) {
                        from = res.getInt("fromAccount");
                    }
                    callback.onReceive(from);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
    public void transactionsIDExists(int transactionsID, Callback<Boolean> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select * from transaction where transactionsID='" + transactionsID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                try {
                    if (res.next()) {
                        callback.onReceive(true);
                    } else {
                        callback.onReceive(false);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
    public void getToByTransactionsID(int transactionID, Callback<Integer> callback) {
        try {
            Connection con = this.main.getSQL().getMySQL().getConnection();
            PreparedStatement preparedStatement = con.prepareStatement("select toAccount from transaction where transactionsID='" + transactionID + "'", java.sql.Statement.RETURN_GENERATED_KEYS);
            main.getSQL().query(preparedStatement, res -> {
                int to = 0;
                try {
                    if (res.next()) {
                        to = res.getInt("toAccount");
                    }
                    callback.onReceive(to);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
}