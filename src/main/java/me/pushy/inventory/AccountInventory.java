package me.pushy.inventory;

import me.pushy.account.Account;
import me.pushy.main.Main;
import me.pushy.player.PlayerHandler;
import me.pushy.transactions.TransactionHandler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Created by Marc / iPushy_
 */
public class AccountInventory {

    private Main main;
    private TransactionHandler transactionHandler;

    private static final String INVENTORY_NAME = "§7Deine Konten";

    public AccountInventory(Main main) {
        this.main = main;
        this.transactionHandler = new TransactionHandler(main);
    }

    /**
     * Returns an inventory with all accounts
     * @param p the player
     * @return the inventory
     */
    public Inventory getAccountInventory(Player p) {
        Inventory inv = Bukkit.createInventory(null, 54, INVENTORY_NAME);
        this.main.availableAccounts.forEach(account -> {
            if (account.getAccountOwner().equals(p)) {
                this.transactionHandler.getAllTransactionsIDs(account.getAccountNumber(), count -> {
                    account.setTransactions(count.size());
                });
            }
        });
        int slot = 0;
        for (int i = 0; i < main.availableAccounts.size(); i++) {
            if (main.availableAccounts.get(i).getAccountOwner().equals(p)) {
                inv.setItem(slot, getAccountItem(main.availableAccounts.get(i), main.availableAccounts.get(i).getTransactions()));
                slot++;
            }
        }
        return inv;
    }

    /**
     * Returns an item with information about the account
     * @param account the account
     * @param transferAmount
     * @return the account item
     */
    private ItemStack getAccountItem(Account account, int transferAmount) {
        ItemStack itemStack = new ItemStack(Material.PAPER);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName("§e#" + account.getAccountNumber());
        itemMeta.setLore(Arrays.asList("§7Kontostand: §e" + account.getCredit(), "§7Transaktionen: §e" + transferAmount));
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }
}
