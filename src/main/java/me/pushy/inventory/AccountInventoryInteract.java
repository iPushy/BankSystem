package me.pushy.inventory;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

/**
 * Created by Marc / iPushy_
 */
public class AccountInventoryInteract implements Listener{

    @EventHandler
    public void onAccountInventoryInteract(InventoryClickEvent e) {
        if (e.getSlotType() == InventoryType.SlotType.OUTSIDE || e.getSlotType() == InventoryType.SlotType.QUICKBAR)
            return;
        if (e.getInventory().getTitle().equalsIgnoreCase("§7Deine Konten")) {
            if (!e.getCurrentItem().hasItemMeta())
                return;
            e.setCancelled(true);
        }
    }
}
