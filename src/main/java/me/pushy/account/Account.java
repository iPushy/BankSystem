package me.pushy.account;

import lombok.Getter;
import lombok.Setter;
import me.pushy.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Marc / iPushy_
 */
public class Account {

    @Getter private UUID playerUUID;
    @Getter @Setter private int credit, accountID, accountNumber, transactions;
    private Main main;

    public Account(Main main, UUID playerUUID, int credit, int accountID, int accountNumber) {
        this.playerUUID = playerUUID;
        this.credit = credit;
        this.accountID = accountID;
        this.main = main;
        this.accountNumber = accountNumber;
    }

    /**
     * Removes a certain amount of money
     *
     * @param amountToRemove the amount of money to remove
     */
    public void removeMoneyFromAccount(int amountToRemove) {
        if (!canMakeTransaction(amountToRemove))
            return;
        setCredit(getCredit() - amountToRemove);
    }

    /**
     * Checks if the account can start a transaction
     *
     * @param amountToRemove the amount to remove during the transaction
     * @return true if the player can start a transaction
     */
    public boolean canMakeTransaction(int amountToRemove) {
        if ((getCredit() - amountToRemove) < 0)
            return false;
        return true;
    }

    /**
     * Adds a certain amount of money to an account
     *
     * @param amountToAdd the amount of money to add
     */
    public void addMoneyToAccount(int amountToAdd) {
        setCredit(getCredit() + amountToAdd);
    }

    /**
     * Checks if the corresponding player is online
     *
     * @return true if the owner is online
     */
    public boolean accountOwnerIsOnline() {
        if (Bukkit.getPlayer(getPlayerUUID()) == null)
            if (Bukkit.getPlayer(getPlayerUUID()) != null)
                return true;
        return false;
    }

    /**
     * Returns the player object of the owner
     *
     * @return the player
     */
    public Player getAccountOwner() {
        return Bukkit.getPlayer(getPlayerUUID());
    }
}
