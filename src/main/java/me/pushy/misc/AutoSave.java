package me.pushy.misc;

import me.pushy.bank.BankHandler;
import me.pushy.main.Main;
import me.pushy.player.PlayerHandler;
import me.pushy.util.Constants;
import org.bukkit.Bukkit;

import static me.pushy.util.Constants.AUTO_SAVE_DELAY;

/**
 * Created by Marc / iPushy_
 */
public class AutoSave {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;

    public AutoSave(Main main) {
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
    }

    public void startAutoSave() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(main, () -> {
            this.main.availableAccounts.forEach(account -> bankHandler.saveCredit(account.getAccountID(), account.getCredit(), cash -> {}));
            Bukkit.getOnlinePlayers().forEach(p -> this.playerHandler.saveData(p));
        }, Constants.AUTO_SAVE_DELAY * 20L, Constants.AUTO_SAVE_CONSTANT * 20L);
    }
}
