package me.pushy.misc;

import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by Marc / iPushy_
 */
public class CommandCooldown {

    public HashMap<String, Long> used = new HashMap<>();

    public boolean canUseCommand(Player p) {
        if (this.used.containsKey(p.getName()) && this.used.get(p.getName()) > System.currentTimeMillis()) return false;
        return true;
    }

    public void useCommand(Player p, int cooldown) {
        this.used.put(p.getName(), (System.currentTimeMillis() + cooldown * 1000));
    }
}
