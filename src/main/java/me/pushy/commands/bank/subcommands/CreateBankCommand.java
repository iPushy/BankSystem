package me.pushy.commands.bank.subcommands;

import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.PlayerHandler;
import me.pushy.util.ArgumentParser;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class CreateBankCommand extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;

    public CreateBankCommand(Main main) {
        super("create", "c");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
    }
    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if (!p.hasPermission("bank.create")) {
            noPermissions(p);
            return true;
        }
        if (!argumentParser.hasExactly(1) || !argumentParser.get(1).equalsIgnoreCase("bank")) {
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /create bank");
            return true;
        }
        this.bankHandler.createBank(p);
        return false;
    }
}
