package me.pushy.commands.bank.subcommands;

import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.PlayerHandler;
import me.pushy.transactions.TransactionHandler;
import me.pushy.util.ArgumentParser;
import me.pushy.util.Tuple;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Marc / iPushy_
 */

public class BankStatementCommand extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;
    private TransactionHandler transactionHandler;

    public BankStatementCommand(Main main) {
        super("statement", "stm", "state");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
        this.transactionHandler = new TransactionHandler(main);
    }
    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if (!p.hasPermission("bank.statement")) {
            noPermissions(p);
            return true;
        }
        if (!argumentParser.hasExactly(1)) {
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /statement <Kontonummer>");
            return true;
        }
        if (!argumentParser.isNumber(args[0])) {
            p.sendMessage(this.main.getPrefix() + "Das ist leider kein gültiger Wert!");
            return true;
        }
        int accountNumber = Integer.valueOf(args[0]);
        if (this.bankHandler.getAccountByAccountNumber(accountNumber) == null) {
            p.sendMessage(this.main.getPrefix() + "Dieses Konto ist leider nicht erreichbar!");
            return true;
        }
        Account account = this.bankHandler.getAccountByAccountNumber(accountNumber);
        if (!account.getAccountOwner().equals(p)) {
            p.sendMessage(this.main.getPrefix() + "Du bist nicht der Besitzer dieses Accounts!");
            return true;
        }
        p.sendMessage(this.main.getPrefix() + "Drucke Kontoauszug...");

        this.transactionHandler.getLimitTransactionsID(accountNumber, limit -> {
            Map<Integer, Tuple<Integer, Integer>> statement = new HashMap<>();
            List<Integer> waiting = limit;
            limit.forEach(i -> {
                this.transactionHandler.getFromByTransactionsID(i, from -> {
                    this.transactionHandler.getToByTransactionsID(i, to -> {
                        statement.put(i, new Tuple<>(from, to));
                    });
                });
            });
            Bukkit.getScheduler().scheduleSyncDelayedTask(main, () -> {
                p.sendMessage(main.getPrefix() + "§7Konto: §e" + accountNumber + "\n");
                waiting.forEach(i -> p.sendMessage(main.getPrefix() + "§e" + statement.get(i).getFirst() + " §8-> §e" + statement.get(i).getSecond() + "\n"));
            }, 100L);

        });
        return false;
    }
}