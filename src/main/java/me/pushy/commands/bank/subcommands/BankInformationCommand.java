package me.pushy.commands.bank.subcommands;

import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.PlayerHandler;
import me.pushy.util.ArgumentParser;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class BankInformationCommand extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;

    public BankInformationCommand(Main main) {
        super("money", "m");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
    }
    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if(!p.hasPermission("bank.money")){
            noPermissions(p);
            return true;
        }
        if(!argumentParser.hasExactly(1)){
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /money <Kontonummer>");
            return true;
        }
        if(!argumentParser.isNumber(args[0])){
            p.sendMessage(this.main.getPrefix() + "Das ist leider kein gültiger Wert!");
            return true;
        }
        int accountNumber = Integer.valueOf(args[0]);
        if(this.bankHandler.getAccountByAccountNumber(accountNumber) == null){
            p.sendMessage(this.main.getPrefix() + "Dieses Konto ist leider nicht erreichbar!");
            return true;
        }
        Account account = this.bankHandler.getAccountByAccountNumber(accountNumber);
        if(!account.getAccountOwner().equals(p)){
            p.sendMessage(this.main.getPrefix() + "Du bist nicht der Besitzer dieses Accounts!");
            return true;
        }
        p.sendMessage(this.main.getPrefix() + "Konto #" + accountNumber + " §8| §7Guthaben: §e" + account.getCredit());

        return false;
    }
}
