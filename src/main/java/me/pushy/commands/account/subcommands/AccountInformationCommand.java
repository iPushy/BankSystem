package me.pushy.commands.account.subcommands;

import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class AccountInformationCommand extends Command {

    private Main main;
    private PlayerHandler playerHandler;

    public AccountInformationCommand(Main main) {
        super("info", "i");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
    }
    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        if (!p.hasPermission("bank.player.info")) {
            noPermissions(p);
            return true;
        }
        AccountPlayer informationPlayer = this.playerHandler.getUser(p);
        p.sendMessage(this.main.getPrefix() + "§7Dein aktuelles Bargeld: §e" + informationPlayer.getTotalCash());
        return true;
    }
}
