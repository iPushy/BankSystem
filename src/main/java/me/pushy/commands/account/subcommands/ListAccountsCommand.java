package me.pushy.commands.account.subcommands;

import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import me.pushy.util.ArgumentParser;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class ListAccountsCommand extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;

    public ListAccountsCommand(Main main) {
        super("list, l");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
    }
    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if (!p.hasPermission("bank.list")) {
            noPermissions(p);
            return true;
        }
        if (!argumentParser.hasExactly(1) || !argumentParser.get(1).equalsIgnoreCase("accounts")) {
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /list accounts");
            return true;
        }
        AccountPlayer accountPlayer = this.playerHandler.getUser(p);
        if (accountPlayer.getAvailableAccounts().isEmpty()) {
            p.sendMessage(this.main.getPrefix() + "Du hast leider aktuell keine Konten!");
            return true;
        }
        accountPlayer.getAvailableAccounts().forEach(accountID -> {
            Account account = this.bankHandler.getAccountByID(accountID);
            if (account != null) {
                p.sendMessage(this.main.getPrefix() + "§7Konto " + account.getAccountNumber() + " §8| §7Guthaben: §e" + account.getCredit());
            }
        });
        return false;
    }
}
