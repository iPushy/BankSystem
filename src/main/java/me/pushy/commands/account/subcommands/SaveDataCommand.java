package me.pushy.commands.account.subcommands;

import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.misc.CommandCooldown;
import me.pushy.player.PlayerHandler;
import me.pushy.scoreboard.handler.MyScoreboardManager;
import me.pushy.util.ArgumentParser;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class SaveDataCommand extends Command {


    private Main main;
    private PlayerHandler playerHandler;
    private BankHandler bankHandler;
    private CommandCooldown commandCooldown;

    private MyScoreboardManager manager;

    public SaveDataCommand(Main main) {
        super("save", "s");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.commandCooldown = new CommandCooldown();
        this.bankHandler = new BankHandler(main, playerHandler);
        this.manager = new MyScoreboardManager(main);
    }

    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if (!p.hasPermission("player.save")) {
            noPermissions(p);
            return true;
        }
        if (!argumentParser.hasNoArguments()) {
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /save");
            return true;
        }
        if (!this.commandCooldown.canUseCommand(p)) {
            p.sendMessage(this.main.getPrefix() + "Du kannst diesen Befehl nur alle 30 Sekunden benutzen!");
            manager.createAndApplyScoreboardFor(p);
            return true;
        }
        this.commandCooldown.useCommand(p, 30);
        p.sendMessage(this.main.getPrefix() + "Speichere Daten...");

        this.main.availableAccounts.stream().filter(account -> account.getAccountOwner().equals(p))
                .forEach(account -> this.bankHandler.saveCredit(account.getAccountID(), account.getCredit(), cash -> {}));
        this.playerHandler.saveData(p);
        Bukkit.getScheduler().scheduleSyncDelayedTask(main, () -> p.sendMessage(main.getPrefix() + "Deine Daten wurden erfolgreich gespeichert!"), 60L);
        return false;
    }
}
