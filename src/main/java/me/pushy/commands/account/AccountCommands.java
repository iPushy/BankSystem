package me.pushy.commands.account;

import me.pushy.command.Command;
import me.pushy.commands.account.subcommands.AccountInformationCommand;
import me.pushy.commands.account.subcommands.ListAccountsCommand;
import me.pushy.commands.account.subcommands.SaveDataCommand;
import me.pushy.commands.bank.subcommands.BankInformationCommand;
import me.pushy.commands.bank.subcommands.BankStatementCommand;
import me.pushy.commands.bank.subcommands.CreateBankCommand;
import me.pushy.commands.bank.subcommands.DeleteBankComand;
import me.pushy.main.Main;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Marc / iPushy_
 */
public class AccountCommands extends Command {

    private List<Command> commands;
    private Main main;

    public AccountCommands(Main main) {
        super("account", "a");
        this.main = main;
        this.commands = new ArrayList<>();
        Command[] commandArray = {
                new AccountInformationCommand(main),
                new ListAccountsCommand(main),
                new SaveDataCommand(main)};
        this.commands.addAll(Arrays.asList(commandArray));
    }
    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        this.commands.forEach(command -> p.sendMessage(this.main.getPrefix() + command.getCommandName() + " §8>> " + command.getAlias()));
        return false;
    }
}
