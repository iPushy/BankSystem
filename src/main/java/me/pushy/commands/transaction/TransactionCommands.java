package me.pushy.commands.transaction;

import me.pushy.command.Command;
import me.pushy.commands.transaction.subcommands.PayMoneyFromBankToUser;
import me.pushy.commands.transaction.subcommands.PayMoneyToBankCommand;
import me.pushy.commands.transaction.subcommands.PayMoneyToUserUserCommand;
import me.pushy.commands.transaction.subcommands.TransferCommand;
import me.pushy.main.Main;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Marc / iPushy_
 */
public class TransactionCommands extends Command {

    private List<Command> commands;
    private Main main;

    public TransactionCommands(Main main) {
        super("transaction", "trans", "t");
        this.main = main;
        this.commands = new ArrayList<>();
        Command[] commandArray = {
                new PayMoneyFromBankToUser(main),
                new PayMoneyToUserUserCommand(main),
                new PayMoneyToBankCommand(main),
                new TransferCommand(main)};
        this.commands.addAll(Arrays.asList(commandArray));
    }

    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        this.commands.forEach(command -> p.sendMessage(this.main.getPrefix() + command.getCommandName() + " §8>> " + command.getAlias()));
        return false;
    }
}
