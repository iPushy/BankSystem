package me.pushy.commands.transaction.subcommands;

import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import me.pushy.transactions.TransactionHandler;
import me.pushy.util.ArgumentParser;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class TransferCommand extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;
    private TransactionHandler transactionHandler;

    public TransferCommand(Main main) {
        super("transfer");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
        this.transactionHandler = new TransactionHandler(main);
    }


    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        boolean[] wasLoaded = {true};

        if (!p.hasPermission("bank.transaction")) {
            noPermissions(p);
            return true;
        }
        if (!argumentParser.hasExactly(3)) {
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /transfer <DeineKontoKontonummer> <EmpfängerKontonummer> <Anzahl>");
            return true;
        }
        if (!argumentParser.isNumber(args[0]) || !argumentParser.isNumber(args[1]) || !argumentParser.isNumber(args[2])) {
            p.sendMessage(this.main.getPrefix() + "Das ist leider kein gültiger Wert!");
            return true;
        }
        if (Integer.valueOf(args[0]) <= 0 || Integer.valueOf(args[1]) <= 0 || Integer.valueOf(args[2]) <= 0) {
            p.sendMessage(this.main.getPrefix() + "Der Wert muss über 0 liegen!");
            return true;
        }

        int fromAccount = Integer.valueOf(args[0]);
        int toAccount = Integer.valueOf(args[1]);
        int valuePayment = Integer.valueOf(args[2]);

        if (!this.bankHandler.accountIsLoadedByAccountNumber(fromAccount)) {
            p.sendMessage(this.main.getPrefix() + "Dieses Konto gehört dir nicht!");
            return true;
        }
        Account from = this.bankHandler.getAccountByAccountNumber(fromAccount);
        if (!from.getAccountOwner().equals(p)) {
            p.sendMessage(this.main.getPrefix() + "Du bist nicht der Besitzer dieses Accounts!");
            return true;
        }
        if (!from.canMakeTransaction(valuePayment)) {
            p.sendMessage(this.main.getPrefix() + "Du hast zu wenig Guthaben auf deinem Konto!");
            return true;
        }
        if (!this.bankHandler.accountIsLoadedByAccountNumber(toAccount)) {
            this.bankHandler.loadAccount(toAccount);
            wasLoaded[0] = false;
        }
        p.sendMessage(this.main.getPrefix() + "Verbinde zu Konten...");
        Bukkit.getScheduler().scheduleSyncDelayedTask(main, () -> {
            if (bankHandler.getAccountByAccountNumber(toAccount) == null) {
                p.sendMessage(main.getPrefix() + "Das angegebene Konto gibt es leider nicht!");
                return;
            }
            Account accountTo = bankHandler.getAccountByAccountNumber(toAccount);
            int currentCashTo = accountTo.getCredit();
            int accountIDTO = accountTo.getAccountID();
            int currentCashFrom = from.getCredit();
            int accountIDFrom = from.getAccountID();
            bankHandler.saveCredit(accountIDTO, currentCashTo + valuePayment, cash -> {
            });
            bankHandler.saveCredit(accountIDFrom, currentCashFrom - valuePayment, cash -> {
            });

            transactionHandler.onAddNewTransaction(from.getAccountNumber(), from.getAccountNumber(), accountTo.getAccountNumber());
            transactionHandler.onAddNewTransaction(accountTo.getAccountNumber(), from.getAccountNumber(), accountTo.getAccountNumber());

            if (!accountTo.getAccountOwner().equals(null)) {
                accountTo.getAccountOwner().sendMessage(main.getPrefix() + "Dir wurde §e" + valuePayment + " §7auf dein Konto §e" + toAccount + " §7von dem Konto §e" + fromAccount + " §7überwiesen!");
            }
            p.sendMessage(main.getPrefix() + "Du hast §e" + valuePayment + " §7von dem Konto §e" + fromAccount + " §7auf das Konto §e" + toAccount + " §7überwiesen!");
            if (!wasLoaded[0]) {
                bankHandler.unloadAccount(toAccount);
            }
        }, 80L);
        return false;
    }

    private void success(Player p, int valuePayment, int accountNumber) {
        Account account = this.bankHandler.getAccountByAccountNumber(accountNumber);
        AccountPlayer accountPlayer = this.playerHandler.getUser(p);
        accountPlayer.makePayment(valuePayment);
        account.addMoneyToAccount(valuePayment);
        p.sendMessage(this.main.getPrefix() + "Du hast §e" + valuePayment + " §7auf das Konto §e" + account.getAccountNumber() + " §7überwiesen!");
    }
}