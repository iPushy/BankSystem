package me.pushy.commands.transaction.subcommands;

import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import me.pushy.util.ArgumentParser;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class PayMoneyToUserUserCommand extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;

    public PayMoneyToUserUserCommand(Main main) {
        super("pay", "p");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
    }

    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if(!p.hasPermission("player.pay")){
            noPermissions(p);
            return true;
        }
        if(!argumentParser.hasExactly(2)){
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /pay <Spieler> <Anzahl>");
            return true;
        }
        if(!argumentParser.isNumber(args[1])){
            p.sendMessage(this.main.getPrefix() + "Das ist leider kein gültiger Wert!");
            return true;
        }
        if(Integer.valueOf(args[1]) <= 0){
            p.sendMessage(this.main.getPrefix() + "Der Wert muss über 0 liegen!");
            return true;
        }
        AccountPlayer accountPlayer = this.playerHandler.getUser(p);
        accountPlayer.makePlayerPayment(Bukkit.getPlayer(args[0]), Integer.valueOf(args[1]));
        return false;
    }
}