package me.pushy.commands.transaction.subcommands;

import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import me.pushy.util.ArgumentParser;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class PayMoneyFromBankToUser extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;

    public PayMoneyFromBankToUser(Main main) {
        super("get", "g");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
    }

    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if (!p.hasPermission("bank.get")) {
            noPermissions(p);
            return true;
        }
        if (!argumentParser.hasExactly(2)) {
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /get <KontoNummer> <Anzahl>");
            return true;
        }
        if (!argumentParser.isNumber(args[0]) || !argumentParser.isNumber(args[1])) {
            p.sendMessage(this.main.getPrefix() + "Das ist leider kein gültiger Wert!");
            return true;
        }
        if (Integer.valueOf(args[0]) <= 0 || Integer.valueOf(args[1]) <= 0) {
            p.sendMessage(this.main.getPrefix() + "Der Wert muss über 0 liegen!");
            return true;
        }
        int accountNumber = Integer.valueOf(args[0]);

        if (!this.bankHandler.accountIsLoadedByAccountNumber(accountNumber)) {
            p.sendMessage(this.main.getPrefix() + "Dieses Konto gehört dir leider nicht!");
            return true;
        }

        Account account = this.bankHandler.getAccountByAccountNumber(accountNumber);
        if (!account.getAccountOwner().equals(p)) {
            p.sendMessage(this.main.getPrefix() + "Dieses Konto gehört dir leider nicht!");
            return true;
        }

        int value = Integer.valueOf(args[1]);
        if (!account.canMakeTransaction(value)) {
            p.sendMessage(this.main.getPrefix() + "Du hast zu wenig Guthaben auf deinem Konto! Bitte versuche /bank <KontoNummer> <Anzahl>");
            return true;
        }
        account.removeMoneyFromAccount(value);
        AccountPlayer accountPlayer = this.playerHandler.getUser(p);
        accountPlayer.setTotalCash(accountPlayer.getTotalCash() + value);
        p.sendMessage(this.main.getPrefix() + "Du hast erfolgreich §e" + value + " §7von dem Konto §e" + accountNumber + " §7abgebucht!");
        return false;
    }
}