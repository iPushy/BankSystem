package me.pushy.commands.transaction.subcommands;

import me.pushy.account.Account;
import me.pushy.bank.BankHandler;
import me.pushy.command.Command;
import me.pushy.main.Main;
import me.pushy.player.AccountPlayer;
import me.pushy.player.PlayerHandler;
import me.pushy.util.ArgumentParser;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Marc / iPushy_
 */
public class PayMoneyToBankCommand extends Command {

    private Main main;
    private BankHandler bankHandler;
    private PlayerHandler playerHandler;

    public PayMoneyToBankCommand(Main main) {
        super("insert", "ins");
        this.main = main;
        this.playerHandler = new PlayerHandler(main);
        this.bankHandler = new BankHandler(main, playerHandler);
    }
    @Override
    public boolean execute(CommandSender sender, org.bukkit.command.Command cmd, String[] args) {
        if (!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ArgumentParser argumentParser = new ArgumentParser(args);

        if (!p.hasPermission("bank.pay")) {
            noPermissions(p);
            return true;
        }
        if (!argumentParser.hasExactly(2)) {
            p.sendMessage(this.main.getPrefix() + "Bitte versuche /insert <KontoNummer> <Anzahl>");
            return true;
        }
        if (!argumentParser.isNumber(args[0]) || !argumentParser.isNumber(args[1])) {
            p.sendMessage(this.main.getPrefix() + "Das ist leider kein gültiger Wert!");
            return true;
        }
        if (Integer.valueOf(args[0]) <= 0 || Integer.valueOf(args[1]) <= 0) {
            p.sendMessage(this.main.getPrefix() + "Der Wert muss über 0 liegen!");
            return true;
        }
        AccountPlayer accountPlayer = this.playerHandler.getUser(p);

        int accountNumber = Integer.valueOf(args[0]);
        int valuePayment = Integer.valueOf(args[1]);

        if (!accountPlayer.canMakePayment(valuePayment)) {
            p.sendMessage(this.main.getPrefix() + "Du hast dafür leider zu wenig Geld dabei!");
            return true;
        }

        if (this.bankHandler.accountIsLoadedByAccountNumber(accountNumber)) {
            success(p, valuePayment, accountNumber);
            return true;
        }
        this.bankHandler.loadAccount(accountNumber);
        p.sendMessage(this.main.getPrefix() + "Verbinde zu Konto...");

        Bukkit.getScheduler().scheduleSyncDelayedTask(main, () -> {
            if (bankHandler.getAccountByAccountNumber(accountNumber) == null) {
                p.sendMessage(main.getPrefix() + "Dieses Konto gibt es leider nicht!");
                return;
            }
            Account account = bankHandler.getAccountByAccountNumber(accountNumber);
            int currentCash = account.getCredit();
            int accountID = account.getAccountID();
            bankHandler.unloadAccount(accountID);
            bankHandler.saveCredit(accountID, currentCash + valuePayment, cash -> {});
            p.sendMessage(main.getPrefix() + "Du hast §e" + valuePayment + " §7auf das Konto §e" + accountNumber + " §7überwiesen!");
        }, 80L);

        return false;
    }
    private void success(Player p, int valuePayment, int accountNumber) {
        Account account = this.bankHandler.getAccountByAccountNumber(accountNumber);
        AccountPlayer accountPlayer = this.playerHandler.getUser(p);
        accountPlayer.makePayment(valuePayment);
        account.addMoneyToAccount(valuePayment);
        p.sendMessage(this.main.getPrefix() + "Du hast §e" + valuePayment + " §7auf das Konto §e" + account.getAccountNumber() + " §7überwiesen!");
    }
}